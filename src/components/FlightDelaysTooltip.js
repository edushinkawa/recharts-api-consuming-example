import React, { Component } from "react"
import Utils from "../utils/Utils"

export default class FlightDelaysTooltip extends Component {
  getIntroOfPage(label) {
    return Utils.getFormattedDate(label)
  }

  render() {
    const { active } = this.props

    if (active) {
      const { payload, label } = this.props
      return (
        <div className="custom-tooltip">
          <p>{`On flights with departure time at ${this.getIntroOfPage(
            label
          )} (${this.props.origin} - ${this.props.destination}):`}</p>
          <ul className="list-group">
            <li className="list-group-item  list-group-item-info">
              The sum of minutes of delayed flights was {payload[0].value}
            </li>
            <li className="list-group-item  list-group-item-info">{`The average amount of delayed minutes was ${payload[1].value.toFixed(
              0
            )}`}</li>
            <li className="list-group-item  list-group-item-info">{`The number of flights with delays was ${payload[1]
              .payload.totalDelayedFlights} (${(payload[1].payload
              .totalDelayedFlights /
              payload[1].payload.totalFlightByDepTime *
              100
            ).toFixed(0)}% of total)`}</li>
            <li className="list-group-item  list-group-item-info">{`The number of flights on this departure time was ${payload[1]
              .payload.totalFlightByDepTime}`}</li>
          </ul>
        </div>
      )
    }

    return null
  }
}
