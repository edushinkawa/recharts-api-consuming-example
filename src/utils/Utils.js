const exports = (module.exports = {
  getFormattedDate: time => {
    const timeFormatted = time.toString()
    const obj = {
      2: `0${timeFormatted.slice(0, 1)}:${timeFormatted.slice(1, 2)}0`,
      3: `0${timeFormatted.slice(0, 1)}:${timeFormatted.slice(1, 3)}`,
      4: `${timeFormatted.slice(0, 2)}:${timeFormatted.slice(2, 4)}`
    }
    return obj[timeFormatted.length]
  },

  filterArgs: (data, param) =>
    data
      .filter(item => item[param])
      .map(item => item[param])
      .filter((elem, index, self) => index === self.indexOf(elem))
})
