import React, { Component } from "react"
import data from "../data/data.json"
import logo from "../images/logo.svg"
import {
  ResponsiveContainer,
  ComposedChart,
  Line,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  defs,
  linearGradient,
  Area,
  AreaChart
} from "recharts"
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css"
import "../App.css"

import FlightDelaysTooltip from "./FlightDelaysTooltip"
import FlightDelaysRatioTooltip from "./FlightDelaysRatioTooltip"
import Utils from "../utils/Utils"

class Chart extends Component {
  constructor() {
    super()
    this.state = {
      data: data,
      paramOrigin: data[0].ORIGIN,
      paramDest: data[0].DEST,
      origin: data[0].ORIGIN,
      destination: data[0].DEST,
      filteredData: [],
      delayByDist: [],
      searchType: "Flight Delays",
      search: false,
      overallRatio: 0
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount = () => {
    const filtered = this.getDelaysTotal(this.getFlights())
    const delayByDist = this.getDelaysByDistante(this.state.data)
    this.setState({ filteredData: filtered, delayByDist: delayByDist })
  }

  getFlights = () => {
    const allFlights = this.state.data.filter(
      item =>
        item.ORIGIN === this.state.origin &&
        item.DEST === this.state.destination
    )
    return allFlights
  }

  getDelayedFlights = () => {
    const delayedFlights = this.state.data.filter(
      item =>
        item.ORIGIN === this.state.origin &&
        item.DEST === this.state.destination &&
        item.ARR_DELAY > 0
    )
    return delayedFlights
  }

  countFlightByDepTime = (arr, depTime) => {
    return arr.filter(item => item.CRS_DEP_TIME === depTime).length
  }

  countDelayedFlightByDepTime = (arr, depTime) => {
    return arr.filter(
      item => item.CRS_DEP_TIME === depTime && item.ARR_DELAY > 0
    ).length
  }

  sumAllValuesObj = (arr, objProp) => {
    return arr.map(item => item[objProp]).reduce((prev, next) => prev + next)
  }

  getDelaysTotal = arr => {
    let result = []
    const totalFlights = this.getFlights(arr).length
    const totalDelayedFlights = this.getDelayedFlights(arr).length

    arr.forEach(item => {
      const depTime = item.CRS_DEP_TIME
      if (!this[depTime]) {
        const totalFlightByDepTime = this.countFlightByDepTime(arr, depTime)
        const totalDelayedFlightByDepTime = this.countDelayedFlightByDepTime(
          arr,
          depTime
        )
        this[depTime] = {
          CRS_DEP_TIME: depTime,
          ARR_DELAY: 0,
          averageDelayTime: 0,
          totalFlightByDepTime: totalFlightByDepTime,
          totalDelayedFlights: totalDelayedFlightByDepTime,
          CRS_ELAPSED_TIME: 0,
          ratio: 0
        }
        result.push(this[depTime])
      }
      this[depTime].ARR_DELAY += Math.abs(item.ARR_DELAY)
      this[depTime].CRS_ELAPSED_TIME += item.CRS_ELAPSED_TIME
      this[depTime].ratio =
        this[depTime].ARR_DELAY / this[depTime].CRS_ELAPSED_TIME * 100
      this[depTime].averageDelayTime = Math.abs(
        this[depTime].ARR_DELAY / this[depTime].totalFlightByDepTime
      )
    }, Object.create(null))
    const formatResult = result.sort(function(a, b) {
      return a.CRS_DEP_TIME - b.CRS_DEP_TIME
    })
    this.setState({
      overallRatio:
        this.sumAllValuesObj(formatResult, "ARR_DELAY") /
        this.sumAllValuesObj(formatResult, "CRS_ELAPSED_TIME")
    })

    return formatResult
  }

  getDelaysByDistante = arr => {
    const data = arr.filter(item => item.ARR_DELAY > 0)
    var r = {}
    data.forEach(function(o) {
      r[o.DISTANCE] = (r[o.DISTANCE] || 0) + o.ARR_DELAY
    })

    const result = Object.keys(r).map(function(k) {
      return { DISTANCE: k, ARR_DELAY: r[k] / 1000 }
    })
    return result
  }

  handleChange = event => {
    event.preventDefault()
    let obj = {}
    obj[event.target.name] = event.target.value
    this.setState(...this.state, obj)
  }

  handleSubmit = event => {
    event.preventDefault()
    const filtered = this.getDelaysTotal(this.getFlights())
    this.setState({
      search: true,
      filteredData: filtered,
      paramOrigin: this.state.origin,
      paramDest: this.state.destination
    })
  }

  render() {
    const {
      data,
      filteredData,
      origin,
      destination,
      paramOrigin,
      paramDest,
      searchType,
      search,
      overallRatio,
      delayByDist
    } = this.state
    const inputValuesOriginData = Utils.filterArgs(data, "ORIGIN")
    const inputValuesDestData = Utils.filterArgs(data, "DEST")
    const InputValuesOrigin = () => (
      <form onSubmit={this.handleSubmit} className="form-inline">
        <div className="form-group">
          <img src={logo} />
          <div>
            <label>
              Origin:
              <select
                type="text"
                value={origin}
                onChange={this.handleChange}
                name="origin"
                aria-label="Flight Origin Input"
                aria-required="true"
              >
                {inputValuesOriginData
                  .sort()
                  .map((item, index) => <option key={index}>{item}</option>)}
              </select>
            </label>
            <label>
              Dest:
              <select
                type="text"
                value={destination}
                onChange={this.handleChange}
                name="destination"
                aria-label="Flight Destination Input"
                aria-required="true"
              >
                {inputValuesDestData
                  .sort()
                  .map((item, index) => <option key={index}>{item}</option>)}
              </select>
            </label>
            <input type="submit" value="Submit" className="btn" />
          </div>
        </div>
      </form>
    )

    const FlightDelaysHistogram = () => (
      <section>
      <h3>Flight Delays according to Departure Date {paramOrigin}-{paramDest}</h3>
        <ResponsiveContainer width="100%" height={400}>
          <ComposedChart data={filteredData}>
            <XAxis
              dataKey="CRS_DEP_TIME"
              //angle={-90}
              angle={-17}
              tickFormatter={Utils.getFormattedDate}
              height={50}
            />
            <YAxis
              label={
                <AxisLabel axisType="yAxis" x={25} y={125} width={0} height={0}>
                  Minutes
                </AxisLabel>
              }
            />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip
              content={
                <FlightDelaysTooltip
                  origin={origin}
                  destination={destination}
                />
              }
            />
            <Legend />
            <Bar
              dataKey="ARR_DELAY"
              fill="#50BFE6"
              name={`Total amount of delayed minutes - By Departure Date`}
            />
            <Area
              type="monotone"
              dataKey="averageDelayTime"
              stroke="#8884d8"
              fill="#8884d8"
              name={`Average amount of delayed minutes - By Departure Date`}
            />
          </ComposedChart>
        </ResponsiveContainer>
      </section>
    )

    const FlightDelaysRatioHistogram = () => (
      <section>
      <h3>Delay Ratios according to Departure Date {paramOrigin}-{paramDest}</h3>
        <ResponsiveContainer width="100%" height={400}>
          <ComposedChart data={filteredData}>
            <XAxis
              dataKey="CRS_DEP_TIME"
              //angle={-90}
              angle={-17}
              tickFormatter={Utils.getFormattedDate}
              height={50}
            />
            <YAxis
              label={
                <AxisLabel axisType="yAxis" x={25} y={125} width={0} height={0}>
                  Ratio (in %)
                </AxisLabel>
              }
            />
            <CartesianGrid strokeDasharray="3 3" />]
            <Tooltip
              content={
                <FlightDelaysRatioTooltip
                  origin={origin}
                  destination={destination}
                />
              }
            />
            <Legend />
            <Bar
              dataKey="ratio"
              fill="#0B468D"
              name={`Flight Delay Ratio for flights in Jan/2017 - By Departure Date`}
            />
          </ComposedChart>
        </ResponsiveContainer>
      </section>
    )

    const DelayByDistance = () => (
      <section>
        <h3>Histogram correlating delays and distances (for all flights in Jan/2017) </h3>
        <ResponsiveContainer width="100%" height={400}>
          <AreaChart
            width={600}
            height={400}
            data={delayByDist}
            margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
          >
            <XAxis
              dataKey="DISTANCE"
              //angle={-90}
              angle={-17}
              height={50}
            />
            <Tooltip />
            <YAxis
              label={
                <AxisLabel axisType="yAxis" x={25} y={125} width={0} height={0}>
                  In 1000 minutes
                </AxisLabel>
              }
            />
            <CartesianGrid strokeDasharray="3 3" />]
            <Legend />
            <Area
              type="monotone"
              dataKey="ARR_DELAY"
              stroke="#8884d8"
              fill="#8884d8"
              name="Distance (in miles)"
            />
          </AreaChart>
        </ResponsiveContainer>
      </section>
    )
    const OverallRatio = () => (
      <h4>
        The overall ratio in Jan/2017 for {paramOrigin}-{paramDest} is {(overallRatio * 100).toFixed(2)}%
      </h4>
    )

    const AxisLabel = ({ axisType, x, y, width, height, stroke, children }) => {
      const isVert = axisType === "yAxis"
      const cx = isVert ? x : x + width / 2
      const cy = isVert ? height / 2 + y : y + height + 10
      const rot = isVert ? `270 ${cx} ${cy}` : 0
      return (
        <text
          x={cx}
          y={cy}
          transform={`rotate(${rot})`}
          textAnchor="middle"
          stroke={stroke}
        >
          {children}
        </text>
      )
    }

    const NotFound = () => (
      <p>
        {`No flights in January were found for ${paramOrigin} - ${paramDest}`}.
      </p>
    )

    return (
      <section className="SearchSection">
        <InputValuesOrigin />
        {filteredData.length > 0 ? (
          <section>
            <FlightDelaysHistogram />
            <FlightDelaysRatioHistogram />
            <OverallRatio />
          </section>
        ) : (
          <NotFound />
        )}
        <hr />
        <DelayByDistance />
      </section>
    )
  }
}

export default Chart
