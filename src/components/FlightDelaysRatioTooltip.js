import React, { Component } from "react"
import Utils from "../utils/Utils"

export default class FlightDelaysRatioTooltip extends Component {
  getIntroOfPage(label) {
    return Utils.getFormattedDate(label)
  }

  render() {
    const { active } = this.props

    if (active) {
      const { payload, label } = this.props
      return (
        <div className="custom-tooltip">
          <p>{`On flights with departure time at ${this.getIntroOfPage(
            label
          )} (${this.props.origin} - ${this.props.destination}):`}</p>
          <ul className="list-group">
            <li className="list-group-item  list-group-item-info">
              The average delay ratio for departure time at{" "}
              {this.getIntroOfPage(label)} is {payload[0].value.toFixed(2)}%
            </li>
            <li className="list-group-item  list-group-item-info">
              The average delay time is{" "}
              {(payload[0].payload.ARR_DELAY /
                payload[0].payload.totalFlightByDepTime
              ).toFixed(0)}{" "}
              minutes
            </li>
            <li className="list-group-item  list-group-item-info">
              The average Elapsed Time is{" "}
              {(payload[0].payload.CRS_ELAPSED_TIME /
                payload[0].payload.totalFlightByDepTime
              ).toFixed(0)}
            </li>
          </ul>
        </div>
      )
    }

    return null
  }
}
